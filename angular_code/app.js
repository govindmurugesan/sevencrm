angular.module('crm',['ngRoute', 'ui.bootstrap', 'moment-picker'])
				.config(['momentPickerProvider', function(momentPickerProvider) {
        momentPickerProvider.options({
            leftArrow: '&lt;',
            rightArrow: '&gt;',
            secondsEnd: 0
        });
    }])