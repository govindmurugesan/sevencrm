angular
    .module('crm')
    .controller('vendorController', vendorController);

vendorController.$inject = ['$scope', 'vendorService'];

function vendorController($scope, vendorService) {
     vendorControllerScope.user = {};

  vendorControllerScope.addVendor = addVendor;
  vendorControllerScope.deleteVendor = deleteVendor;
  vendorControllerScope.editVendor = editVendor;
  vendorControllerScope.getVendor = getVendor;

  console.log('vendor controller', vendorControllerScope);

  function addVendor() {
    if (vendorControllerScope.user != null) {
      vendorService.addVendor(vendorControllerScope.user).then(function(res) {
        if (res) {
          console.log('addVendor res', res);
        }
      }, function(err) {
        console.log('addVendor err', err);
      });
    }
  }

  function deleteVendor(data) {
    if (data) {
      vendorService.editVendor(data).then(function(res) {
        if (res) {
          console.log('editVendor res', res);
        }
      }, function(err) {
        console.log('editVendor err', err);
      });
    }
  }

  function editVendor(data) {
    if (data) {
      vendorService.editVendor(data).then(function(res) {
        if (res) {
          console.log('editVendor res', res);
        }
      }, function(err) {
        console.log('editVendor err', err);
      });
    }
  }

  function getVendor() {
    vendorService.getVendor().then(function(res) {
      if (res) {
        console.log('editVendor res', res);
      }
    }, function(err) {
      console.log('editVendor err', err);
    });
  }
}