angular
  .module('crm')
  .controller('rolesController', rolesController);

rolesController.$inject = ['$scope', 'rolesService'];

function rolesController($scope, rolesService) {
  var rolesControllerScope = this;

  rolesControllerScope.editRole = true;
  rolesControllerScope.role;

  // rolesControllerScope.moveToRoleType = moveToRoleType;
  rolesControllerScope.addRole = addRole;
  rolesControllerScope.deleteRole = deleteRole;
  rolesControllerScope.editRole = editRole;
  rolesControllerScope.getRole = getRole;

  getRole();

  console.log('Roles controller', rolesControllerScope);

  function addRole() {
    if (rolesControllerScope.role) {
      rolesService.addRole(rolesControllerScope.role).then(function(res) {
        if (res) {
          console.log('addRole res ', res);
        }
      }, function(err) {
        console.log('Err addRole ', err)
      });
    }
  }

  function deleteRole(role) {
    if (role) {
      rolesService.deleteRole(role).then(function(res) {
        if (res) {
          console.log('editRole res ', res);
        }
      }, function(err) {
        console.log('Err editRole ', err)
      });
    }
  }

  function editRole(role) {
    if (role) {
      rolesService.editRole(role).then(function(res) {
        if (res) {
          console.log('editRole res ', res);
        }
      }, function(err) {
        console.log('Err editRole ', err)
      });
    }
  }

  function getRole() {
    rolesService.getRole().then(function(res) {
      if (res) {
        console.log('res in get role ', res);
      }
    }, function(err) {
      console.log('Err getRole ', err)
    });
  }
}
