angular
    .module('crm')
    .controller('employeeController', employeeController);

employeeController.$inject = ['$scope', '$location', '$routeParams', 'employeeService'];

function employeeController($scope, $location, $routeParams, employeeService) {
    var employeeControllerScope = this;

    employeeControllerScope.employee;
    employeeControllerScope.employeeForm;
    employeeControllerScope.employeeList = [];
    employeeControllerScope.dateOfBirthCalender = false;
    employeeControllerScope.startCalender = false;


    employeeControllerScope.addEmployee = addEmployee;
    employeeControllerScope.decline = decline;
    employeeControllerScope.deleteEmployee = deleteEmployee;
    employeeControllerScope.editEmployee = editEmployee;
    employeeControllerScope.getEmployee = getEmployee;
    employeeControllerScope.goToEditEmployee = goToEditEmployee;
    employeeControllerScope.opendateOfBirthCalender = opendateOfBirthCalender;
    employeeControllerScope.openstartCalender = openstartCalender;
    

    if ($routeParams && $routeParams.employeeID) {
        getEmployee($routeParams.employeeID);
    }

    getEmployee();
    console.log('employee controller', employeeControllerScope);
    employeeControllerScope.employeeTypeList = [{ name: "abc" }, { name: "ab1" }, { name: "ab1a" }, { name: "ab1w" }, { name: "abaac" }]

    function addEmployee() {
        console.log('addEmployee');
        if (employeeControllerScope.employee) {
            employeeService.addEmployee(employeeControllerScope.employee).then(function(res) {
                if (res.successflag) {
                    console.log('edited Successfully', res);
                    decline();
                } else
                    console.log('errors in response getEmployee', res.errors);
            }, function(err) {
                console.log('Error while deleting Employee', err)
            });
        } else console.log('employeeControllerScope.employee is undefined');
    }

    function decline() {
        console.log('decline');
        employeeControllerScope.employeeForm.$setPristine();
        employeeControllerScope.employeeForm.$setUntouched();
        employeeControllerScope.employee = null;

    }

    function deleteEmployee(employeeID) {
        console.log('deleteEmployee', employeeID);
        if (employeeID) {
            employeeService.deleteEmployee(employeeID).then(function(res) {
                if (res.successflag) {
                    console.log('edited Successfully', res);
                    getEmployee();
                } else
                    console.log('errors in response getEmployee', res.errors);
            }, function(err) {
                console.log('Error while deleting Employee', err)
            });
        }
    }

    function editEmployee(employeeID) {
        console.log('editEmployee', employeeID);
        if (employeeID) {
            employeeService.editEmployee(employeeID).then(function(res) {
                if (res.successflag) {
                    console.log('edited Successfully', res);
                } else
                    console.log('errors in response getEmployee', res.errors);
            }, function(err) {
                console.log('Error Get Employee', err)
            });
        }
    }

    function getEmployee(data) {
        employeeService.getEmployee(data).then(function(res) {
            if (res.successflag) {
                console.log('getEmployee Successfully', res);
                if (data)
                    employeeControllerScope.employeeList = res.result;
                else
                    employeeControllerScope.employeeList = res.result;
            } else
                console.log('errors in response getEmployee', res.errors);
        }, function(err) {
            console.log('Error Get Employee', err)
        });
    }

    function goToEditEmployee(employeeID) {
        console.log('editEmployee', employeeID);
        if (employeeID)
            $location.path('/editEmployee/' + employeeID);
    }



    function opendateOfBirthCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        employeeControllerScope.dateOfBirthCalender = !employeeControllerScope.dateOfBirthCalender
    };
    function openstartCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        employeeControllerScope.startCalender = !employeeControllerScope.startCalender
    };

function cusDD(select, style) {
  /*Style Switcher*/
  var ddstyle = "";
  if (!style) {
    ddstyle = "cusDD_default";
  } else if (style == "slick dark") {
    ddstyle = "cusDD_slick_d";
  } else if (style == "slick light") {
    ddstyle = "cusDD_slick_l";
  } else {
    ddstyle = style;
  }

  for (var i = 0; i < $(select).length; i++) {
    var curr = $($(select)[i]);
    
    //Replace select with div
    curr.addClass(ddstyle+" cusDD").changeElementType("div");
    
    //put drop downs in a container
    //Replace options with divs
    curr = $($(select)[i]);
    curr.find("option").wrapAll("<div class='cusDD_options' />");
    curr.find("option").addClass("cusDD_opt").each(function() {
      $(this).changeElementType("div");
    });
    
    //Add selector and drop down
    curr.prepend("<div class='cusDD_select'><div class='cusDD_arrow'></div></div>");
    
    //Add default option
    var def = (curr.find("div[selected='selected']").length >= 1) ? $(curr.find("div[selected='selected']")) : $(curr.find(".cusDD_opt")[0]);
    curr.find(".cusDD_select").prepend(def.text());
    
  } //End for loop

  $(document).click(function() {
    $(".cusDD_options").slideUp(200);
    $(".cusDD_arrow").removeClass("active");
  })
  
  $(select).click(function(e) {
    e.stopPropagation();
    $(this).find(".cusDD_options").slideToggle(200);
    $(this).find(".cusDD_arrow").toggleClass("active");
  })
  $(".cusDD_opt").click(function() {
    $($(this).parent()).siblings(".cusDD_select").contents()[0].nodeValue = $(this).text();
  });

  } // End function)

(function($) {
    $.fn.changeElementType = function(newType) {
        var attrs = {};

        $.each(this[0].attributes, function(idx, attr) {
            attrs[attr.nodeName] = attr.nodeValue;
        });

        this.replaceWith(function() {
            return $("<" + newType + "/>", attrs).append($(this).contents());
        });
    };
})(jQuery);

/* Call the cusDD function on any select elements by ID or Class */
$(document).ready(function() {
  cusDD("#select1");
  cusDD("#select2");
  cusDD("#select3");
});
}