angular
  .module('crm')
  .controller('contactController', contactController);

contactController.$inject = ['$scope', '$location', '$routeParams', 'contactService'];

function contactController($scope, $location, $routeParams, contactService) {
  var contactControllerScope = this;

  console.log('contact controller', contactControllerScope);

  contactControllerScope.contact;
  contactControllerScope.contactForm;
  contactControllerScope.contactList = [];

  contactControllerScope.addContact = addContact;
  contactControllerScope.decline = decline;
  contactControllerScope.deleteContact = deleteContact;
  contactControllerScope.editContact = editContact;
  contactControllerScope.getContact = getContact;
  contactControllerScope.goToEditContact = goToEditContact;

  if ($routeParams && $routeParams.contactID) {
    getContact($routeParams.contactID);
  }

  // getContact();
  console.log('contact controller', contactControllerScope);

  function addContact() {
    console.log('addContact');
    if (contactControllerScope.contact) {
      contactService.addContact(contactControllerScope.contact).then(function(res) {
        if (res.successflag) {
          console.log('edited Successfully', res);
          decline();
        } else
          console.log('errors in response getContact', res.errors);
      }, function(err) {
        console.log('Error while deleting Contact', err)
      });
    } else console.log('contactControllerScope.contact is undefined');
  }

  function decline() {
    console.log('decline');
    contactControllerScope.contactForm.$setPristine();
    contactControllerScope.contactForm.$setUntouched();
    contactControllerScope.contact = null;

  }

  function deleteContact(contactID) {
    console.log('deleteContact', contactID);
    if (contactID) {
      contactService.deleteContact(contactID).then(function(res) {
        if (res.successflag) {
          console.log('edited Successfully', res);
          getContact();
        } else
          console.log('errors in response getContact', res.errors);
      }, function(err) {
        console.log('Error while deleting Contact', err)
      });
    }
  }

  function editContact(contactID) {
    console.log('editContact', contactID);
    if (contactID) {
      contactService.editContact(contactID).then(function(res) {
        if (res.successflag) {
          console.log('edited Successfully', res);
        } else
          console.log('errors in response getContact', res.errors);
      }, function(err) {
        console.log('Error Get Contact', err)
      });
    }
  }

  function getContact(data) {
    contactService.getContact(data).then(function(res) {
      if (res.successflag) {
        console.log('getContact Successfully', res);
        if (data)
          contactControllerScope.contact = res.result;
        else
          contactControllerScope.contactList = res.result;
      } else
        console.log('errors in response getContact', res.errors);
    }, function(err) {
      console.log('Error Get Contact', err)
    });
  }

  function goToEditContact(contactID) {
    console.log('editContact', contactID);
    contactID = "5";
    if (contactID)
      $location.path('/editContact/' + contactID);
  }

   function cusDD(select, style) {
  /*Style Switcher*/
  var ddstyle = "";
  if (!style) {
    ddstyle = "cusDD_default";
  } else if (style == "slick dark") {
    ddstyle = "cusDD_slick_d";
  } else if (style == "slick light") {
    ddstyle = "cusDD_slick_l";
  } else {
    ddstyle = style;
  }

  for (var i = 0; i < $(select).length; i++) {
    var curr = $($(select)[i]);
    
    //Replace select with div
    curr.addClass(ddstyle+" cusDD").changeElementType("div");
    
    //put drop downs in a container
    //Replace options with divs
    curr = $($(select)[i]);
    curr.find("option").wrapAll("<div class='cusDD_options' />");
    curr.find("option").addClass("cusDD_opt").each(function() {
      $(this).changeElementType("div");
    });
    
    //Add selector and drop down
    curr.prepend("<div class='cusDD_select'><div class='cusDD_arrow'></div></div>");
    
    //Add default option
    var def = (curr.find("div[selected='selected']").length >= 1) ? $(curr.find("div[selected='selected']")) : $(curr.find(".cusDD_opt")[0]);
    curr.find(".cusDD_select").prepend(def.text());
    
  } //End for loop

  $(document).click(function() {
    $(".cusDD_options").slideUp(200);
    $(".cusDD_arrow").removeClass("active");
  })
  
  $(select).click(function(e) {
    e.stopPropagation();
    $(this).find(".cusDD_options").slideToggle(200);
    $(this).find(".cusDD_arrow").toggleClass("active");
  })
  $(".cusDD_opt").click(function() {
    $($(this).parent()).siblings(".cusDD_select").contents()[0].nodeValue = $(this).text();
  });

  } // End function)

(function($) {
    $.fn.changeElementType = function(newType) {
        var attrs = {};

        $.each(this[0].attributes, function(idx, attr) {
            attrs[attr.nodeName] = attr.nodeValue;
        });

        this.replaceWith(function() {
            return $("<" + newType + "/>", attrs).append($(this).contents());
        });
    };
})(jQuery);

/* Call the cusDD function on any select elements by ID or Class */
$(document).ready(function() {
  cusDD("#select1");
  cusDD("#select2");
  cusDD("#select3");
});
}
