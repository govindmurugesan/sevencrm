angular
    .module('crm')
    .controller('customerController', customerController);

customerController.$inject = ['$scope', '$location', '$routeParams', 'customerService'];

function customerController($scope, $location, $routeParams, customerService) {
    var customerControllerScope = this;

    customerControllerScope.customer;
    customerControllerScope.customerForm;
    customerControllerScope.customerList = [];

    customerControllerScope.addCustomer = addCustomer;
    customerControllerScope.decline = decline;
    customerControllerScope.deleteCustomer = deleteCustomer;
    customerControllerScope.editCustomer = editCustomer;
    customerControllerScope.getCustomer = getCustomer;
    customerControllerScope.goToEditCustomer = goToEditCustomer;

    if ($routeParams && $routeParams.customerID) {
        getCustomer($routeParams.customerID);
    }

    getCustomer();
    console.log('customer controller', customerControllerScope);

    function addCustomer() {
        console.log('addCustomer', customerControllerScope.customer);
        if (customerControllerScope.customer) {
            customerService.addCustomer(customerControllerScope.customer).then(function (res) {
                if (res.successflag) {
                    console.log('edited Successfully', res);
                    decline();
                } else
                    console.log('errors in response getCustomer', res.errors);
            }, function (err) {
                console.log('Error while deleting Customer', err)
            });
        } else console.log('customerControllerScope.customer is undefined');
    }

    function decline() {
        console.log('decline');
        customerControllerScope.customerForm.$setPristine();
        customerControllerScope.customerForm.$setUntouched();
        customerControllerScope.customer = null;
    }

    function deleteCustomer(customerID) {
        console.log('deleteCustomer', customerID);
        if (customerID) {
            customerService.deleteCustomer(customerID).then(function (res) {
                if (res.successflag) {
                    console.log('edited Successfully', res);
                    getCustomer();
                } else
                    console.log('errors in response getCustomer', res.errors);
            }, function (err) {
                console.log('Error while deleting Customer', err)
            });
        }
    }

    function editCustomer(customerID) {
        console.log('editCustomer', customerID);
        if (customerID) {
            customerService.editCustomer(customerID).then(function (res) {
                if (res.successflag) {
                    console.log('edited Successfully', res);
                } else
                    console.log('errors in response getCustomer', res.errors);
            }, function (err) {
                console.log('Error Get Customer', err)
            });
        }
    }

    function getCustomer(data) {
        customerService.getCustomer(data).then(function (res) {
            if (res.successflag) {
                console.log('getCustomer Successfully', res);
                if (data)
                    customerControllerScope.customer = res.result
                else
                    customerControllerScope.customerList = res.result;
            } else
                console.log('errors in response getCustomer', res.errors);
        }, function (err) {
            console.log('Error Get Customer', err)
        });
    }

    function goToEditCustomer(customerID) {
        console.log('editCustomer', customerID);
        if (customerID)
            $location.path('/editCustomer/' + customerID);
    }

    function cusDD(select, style) {
  /*Style Switcher*/
  var ddstyle = "";
  if (!style) {
    ddstyle = "cusDD_default";
  } else if (style == "slick dark") {
    ddstyle = "cusDD_slick_d";
  } else if (style == "slick light") {
    ddstyle = "cusDD_slick_l";
  } else {
    ddstyle = style;
  }

  for (var i = 0; i < $(select).length; i++) {
    var curr = $($(select)[i]);
    
    //Replace select with div
    curr.addClass(ddstyle+" cusDD").changeElementType("div");
    
    //put drop downs in a container
    //Replace options with divs
    curr = $($(select)[i]);
    curr.find("option").wrapAll("<div class='cusDD_options' />");
    curr.find("option").addClass("cusDD_opt").each(function() {
      $(this).changeElementType("div");
    });
    
    //Add selector and drop down
    curr.prepend("<div class='cusDD_select'><div class='cusDD_arrow'></div></div>");
    
    //Add default option
    var def = (curr.find("div[selected='selected']").length >= 1) ? $(curr.find("div[selected='selected']")) : $(curr.find(".cusDD_opt")[0]);
    curr.find(".cusDD_select").prepend(def.text());
    
  } //End for loop

  $(document).click(function() {
    $(".cusDD_options").slideUp(200);
    $(".cusDD_arrow").removeClass("active");
  })
  
  $(select).click(function(e) {
    e.stopPropagation();
    $(this).find(".cusDD_options").slideToggle(200);
    $(this).find(".cusDD_arrow").toggleClass("active");
  })
  $(".cusDD_opt").click(function() {
    $($(this).parent()).siblings(".cusDD_select").contents()[0].nodeValue = $(this).text();
  });

  } // End function)

(function($) {
    $.fn.changeElementType = function(newType) {
        var attrs = {};

        $.each(this[0].attributes, function(idx, attr) {
            attrs[attr.nodeName] = attr.nodeValue;
        });

        this.replaceWith(function() {
            return $("<" + newType + "/>", attrs).append($(this).contents());
        });
    };
})(jQuery);

/* Call the cusDD function on any select elements by ID or Class */
$(document).ready(function() {
  cusDD("#select1");
  cusDD("#select2");
  cusDD("#select3");
  cusDD("#select4");
});

}