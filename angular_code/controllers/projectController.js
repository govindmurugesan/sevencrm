angular
    .module('crm')
    .controller('projectController', projectController);

projectController.$inject = ['$scope', '$location', '$routeParams', 'projectService'];

function projectController($scope, $location, $routeParams, projectService) {
    var projectControllerScope = this;

    projectControllerScope.project;
    projectControllerScope.projectForm;
    projectControllerScope.projectList = [];

    projectControllerScope.endCalender = false;
    projectControllerScope.startCalender = false;

    projectControllerScope.addProject = addProject;
    projectControllerScope.decline = decline;
    projectControllerScope.deleteProject = deleteProject;
    projectControllerScope.editProject = editProject;
    projectControllerScope.getProject = getProject;
    projectControllerScope.goToEditProject = goToEditProject;

    projectControllerScope.openendCalender = openendCalender;
    projectControllerScope.openstartCalender = openstartCalender;

    if ($routeParams && $routeParams.projectID) {
        editProject($routeParams.projectID);
    }

    getProject();
    console.log('project controller', projectControllerScope);

    function addProject() {
        console.log('addProject', projectControllerScope.project);
        if (projectControllerScope.project) {
            projectService.addProject(projectControllerScope.project).then(function (res) {
                if (res.successflag) {
                    console.log('edited Successfully', res);
                    decline();
                } else
                    console.log('errors in response getProject', res.errors);
            }, function (err) {
                console.log('Error while deleting Project', err)
            });
        } else console.log('projectControllerScope.project is undefined');
    }

    function decline() {
        console.log('decline');
        projectControllerScope.projectForm.$setPristine();
        projectControllerScope.projectForm.$setUntouched();
        projectControllerScope.project = null;
    }

    function deleteProject(projectID) {
        console.log('deleteProject', projectID);
        if (projectID) {
            projectService.deleteProject(projectID).then(function (res) {
                if (res.successflag) {
                    console.log('edited Successfully', res);
                    getProject();
                } else
                    console.log('errors in response getProject', res.errors);
            }, function (err) {
                console.log('Error while deleting Project', err)
            });
        }
    }

    function editProject(projectID) {
        console.log('editProject', projectID);
        if (projectID) {
            projectService.editProject(projectID).then(function (res) {
                if (res.successflag) {
                    console.log('edited Successfully', res);
                } else
                    console.log('errors in response getProject', res.errors);
            }, function (err) {
                console.log('Error Get Project', err)
            });
        }
    }

    function getProject(data) {
        projectService.getProject(data).then(function (res) {
            if (res.successflag) {
                console.log('getProject Successfully', res);
                if (data)
                    projectControllerScope.project = res.result
                else
                    projectControllerScope.projectList = res.result;

            } else
                console.log('errors in response getProject', res.errors);
        }, function (err) {
            console.log('Error Get Project', err)
        });
    }

    function goToEditProject(projectID) {
        console.log('editProject', projectID);
        if (projectID)
            $location.path('/editProject/' + projectID);
    }


function openstartCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        projectControllerScope.startCalender = !projectControllerScope.startCalender
    };

    function openendCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        projectControllerScope.endCalender = !projectControllerScope.endCalender
    };

    function cusDD(select, style) {
  /*Style Switcher*/
  var ddstyle = "";
  if (!style) {
    ddstyle = "cusDD_default";
  } else if (style == "slick dark") {
    ddstyle = "cusDD_slick_d";
  } else if (style == "slick light") {
    ddstyle = "cusDD_slick_l";
  } else {
    ddstyle = style;
  }

  for (var i = 0; i < $(select).length; i++) {
    var curr = $($(select)[i]);
    
    //Replace select with div
    curr.addClass(ddstyle+" cusDD").changeElementType("div");
    
    //put drop downs in a container
    //Replace options with divs
    curr = $($(select)[i]);
    curr.find("option").wrapAll("<div class='cusDD_options' />");
    curr.find("option").addClass("cusDD_opt").each(function() {
      $(this).changeElementType("div");
    });
    
    //Add selector and drop down
    curr.prepend("<div class='cusDD_select'><div class='cusDD_arrow'></div></div>");
    
    //Add default option
    var def = (curr.find("div[selected='selected']").length >= 1) ? $(curr.find("div[selected='selected']")) : $(curr.find(".cusDD_opt")[0]);
    curr.find(".cusDD_select").prepend(def.text());
    
  } //End for loop

  $(document).click(function() {
    $(".cusDD_options").slideUp(200);
    $(".cusDD_arrow").removeClass("active");
  })
  
  $(select).click(function(e) {
    e.stopPropagation();
    $(this).find(".cusDD_options").slideToggle(200);
    $(this).find(".cusDD_arrow").toggleClass("active");
  })
  $(".cusDD_opt").click(function() {
    $($(this).parent()).siblings(".cusDD_select").contents()[0].nodeValue = $(this).text();
  });

  } // End function)

(function($) {
    $.fn.changeElementType = function(newType) {
        var attrs = {};

        $.each(this[0].attributes, function(idx, attr) {
            attrs[attr.nodeName] = attr.nodeValue;
        });

        this.replaceWith(function() {
            return $("<" + newType + "/>", attrs).append($(this).contents());
        });
    };
})(jQuery);

/* Call the cusDD function on any select elements by ID or Class */
$(document).ready(function() {
  cusDD("#select1");
});
}