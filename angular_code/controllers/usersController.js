angular
  .module('crm')
  .controller('usersController', usersController);

usersController.$inject = ['$scope', 'usersService'];

function usersController($scope, usersService) {
  var usersControllerScope = this;

  usersControllerScope.user = {};

  usersControllerScope.addUser = addUser;
  usersControllerScope.deleteUser = deleteUser;
  usersControllerScope.editUser = editUser;
  usersControllerScope.getUser = getUser;

  console.log('users controller', usersControllerScope);

  function addUser() {
    if (usersControllerScope.user != null) {
      usersService.addUser(usersControllerScope.user).then(function(res) {
        if (res) {
          console.log('addUser res', res);
        }
      }, function(err) {
        console.log('addUser err', err);
      });
    }
  }

  function deleteUser(data) {
    if (data) {
      usersService.editUser(data).then(function(res) {
        if (res) {
          console.log('editUser res', res);
        }
      }, function(err) {
        console.log('editUser err', err);
      });
    }
  }

  function editUser(data) {
    if (data) {
      usersService.editUser(data).then(function(res) {
        if (res) {
          console.log('editUser res', res);
        }
      }, function(err) {
        console.log('editUser err', err);
      });
    }
  }

  function getUser() {
    usersService.getUser().then(function(res) {
      if (res) {
        console.log('editUser res', res);
      }
    }, function(err) {
      console.log('editUser err', err);
    });
  }

}
