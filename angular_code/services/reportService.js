angular
    .module('crm')
    .service('reportService', reportService);

reportService.$inject = ['constants', 'commonService'];

function reportService(constants, commonService) {

    var reportsService = {
        addreport: addreport,
        getreport: getreport
    };

    return reportsService;

    function addreport(data) {
        console.log('add Report data', data);
        return commonService.apiCall(constants.report, data);
    }

    function getreport() {
        console.log('get Report data');
        return commonService.apiCall(constants.report,undefined,true);
    }
}