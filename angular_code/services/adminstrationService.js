angular
    .module('crm')
    .service('administrationService', administrationService);

administrationService.$inject = ['constants', 'commonService'];

function administrationService(constants, commonService) {

    var administrationsService = {
        addAdministration: addAdministration,
        getAdministration: getAdministration
    };

    return administrationsService;

    function addAdministration(data) {
        console.log('add Administration data', data);
        return commonService.apiCall(constants.administration, data);
    }

    function getAdministration() {
        console.log('get Administration');
        return commonService.apiCall(constants.administration,undefined,true);
    }
}