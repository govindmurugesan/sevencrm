angular
    .module('crm')
    .service('hrService', hrService);

hrService.$inject = ['constants', 'commonService'];

function hrService(constants, commonService) {

    var hrsService = {
        addHr: addHr,
        getHr: getHr
    };

    return hrsService;

    function addHr(data) {
        console.log('add Hr data', data);
        return commonService.apiCall(constants.hr, data);
    }

    function getHr() {
        console.log('get Hr');
        return commonService.apiCall(constants.hr,undefined,true);
    }
}