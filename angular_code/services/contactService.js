angular
    .module('crm')
    .service('contactService', contactService);

contactService.$inject = ['constants', 'commonService'];

function contactService(constants, commonService) {

    var contactsService = {
        addContact: addContact,
        getContact: getContact
    };

    return contactsService;

    function addContact(data) {
        console.log('add Contact data', data);
        return commonService.apiCall(constants.contact, data);
    }

    function getContact(data) {
        console.log('get Contact');
        return commonService.apiCall(constants.contact,undefined,true);
    }
}