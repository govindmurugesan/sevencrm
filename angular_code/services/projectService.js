angular
    .module('crm')
    .service('projectService', projectService);

projectService.$inject = ['constants', 'commonService'];

function projectService(constants, commonService) {
    
    var projectService = {
        addProject: addProject,
        deleteProject: deleteProject,
        editProject: editProject,
        getProject: getProject
      };
    
      return projectService;
    
      function addProject(data) {
        console.log('add Project data', data);
        return commonService.apiCall(constants.projectPersist, data);
      }
    
      function deleteProject(data) {
        console.log('add Project data', data);
        return commonService.apiCall(constants.projectDelete, data);
      }
    
      function editProject(data) {
        console.log('add Project data', data);
        return commonService.apiCall(constants.projectUpdate, data);
      }
    
      function getProject(data) {
        console.log('get Project', data);
        if (!data)
          return commonService.apiCall(constants.projectRetrieve,undefined,true);
        else
          return commonService.apiCall(constants.projectRetrieve, data);
      }
}