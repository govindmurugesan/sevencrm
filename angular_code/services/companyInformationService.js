angular
    .module('crm')
    .service('companyInformationService', companyInformationService);

companyInformationService.$inject = ['constants', 'commonService'];

function companyInformationService(constants, commonService) {

    var companyInformationsService = {
        addCompanyInformation: addCompanyInformation,
        getCompanyInformation: getCompanyInformation
    };

    return companyInformationsService;

    function addCompanyInformation(data) {
        console.log('add CompanyInformation data', data);
        return commonService.apiCall(constants.companyInformation, data);
    }

    function getCompanyInformation() {
        console.log('get CompanyInformation');
        return commonService.apiCall(constants.companyInformation,undefined,true);
    }
}