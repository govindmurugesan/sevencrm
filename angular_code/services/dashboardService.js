angular
    .module('crm')
    .service('dashboardService', dashboardService);

dashboardService.$inject = ['constants', 'commonService'];

function dashboardService(constants, commonService) {

    var dashboardService = {
        addDashboard: addDashboard,
        getDashboard: getDashboard
    };

    return dashboardService;

    function addDashboard(data) {
        console.log('add Dashboard data', data);
        return commonService.apiCall(constants.dashboard, data);
    }

    function getDashboard() {
        console.log('get Dashboard');
        return commonService.apiCall(constants.dashboard,undefined,true);
    }
}