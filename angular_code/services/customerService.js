angular
  .module('crm')
  .service('customerService', customerService);

customerService.$inject = ['constants', 'commonService'];

function customerService(constants, commonService) {

  var customerService = {
    addCustomer: addCustomer,
    deleteCustomer: deleteCustomer,
    editCustomer: editCustomer,
    getCustomer: getCustomer
  };

  return customerService;

  function addCustomer(data) {
    console.log('add Customer data', data);
    return commonService.apiCall(constants.customerPersist, data);
  }

  function deleteCustomer(data) {
    console.log('add Customer data', data);
    return commonService.apiCall(constants.customerDelete, data);
  }

  function editCustomer(data) {
    console.log('add Customer data', data);
    return commonService.apiCall(constants.customerUpdate, data);
  }

  function getCustomer(data) {
    console.log('get Customer', data);
    if (!data)
      return commonService.apiCall(constants.customerRetrieve,undefined,true);
    else
      return commonService.apiCall(constants.customerRetrieve, data);
  }
}
