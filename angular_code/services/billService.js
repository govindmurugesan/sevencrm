angular
    .module('crm')
    .service('billService', billService);

billService.$inject = ['constants', 'commonService'];

function billService(constants, commonService) {

    var billsService = {
        addBill: addBill,
        getBill: getBill
    };

    return billsService;

    function addBill(data) {
        console.log('add Bill data', data);
        return commonService.apiCall(constants.bill, data);
    }

    function getBill() {
        console.log('get Bill');
        return commonService.apiCall(constants.bill,undefined,true);
    }
}