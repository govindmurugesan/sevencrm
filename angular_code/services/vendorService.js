angular
  .module('crm')
  .service('vendorService', vendorService);

vendorService.$inject = ['constants', 'commonService'];

function vendorService(constants, commonService) {

  var vendorService = {
    addVendor: addVendor,
    deleteVendor: deleteVendor,
    editVendor: editVendor,
    getVendor: getVendor
  };

  return vendorService;

  function addVendor(data) {
    console.log('add Vendor data', data);
    return commonService.apiCall(constants.vendorPersist, data);
  }

  function deleteVendor(data) {
    console.log('add Vendor data', data);
    return commonService.apiCall(constants.vendorDelete, data);
  }

  function editVendor(data) {
    console.log('add Vendor data', data);
    return commonService.apiCall(constants.vendorUpdate, data);
  }

  function getVendor(data) {
    console.log('get Vendor', data);
    if (!data)
      return commonService.apiCall(constants.vendorRetrieve,undefined,true);
    else
      return commonService.apiCall(constants.vendorRetrieve, data);
  }
}
