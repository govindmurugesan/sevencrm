angular
    .module('crm')
    .factory('commonService', commonService);

commonService.$inject = ['$q', '$http'];

function commonService($q, $http) {

    var commonService = {
        apiCall: apiCall,
    };

    return commonService;

    function apiCall(uri, data, getAll) {
        var deferred = $q.defer();
        if (data || getAll) {
            $http({
                method: "GET",
                url: uri,
                params: data
            }).then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                console.log('epi error ', error);
                deferred.reject(error);
            });
        } else deferred.reject({
            msg: 'Data is undefined'
        });
        return deferred.promise;

    }
}