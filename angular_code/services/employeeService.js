angular
    .module('crm')
    .service('employeeService', employeeService);

employeeService.$inject = ['constants', 'commonService'];

function employeeService(constants, commonService) {

   var employeeService = {
    addEmployee: addEmployee,
    deleteEmployee: deleteEmployee,
    editEmployee: editEmployee,
    getEmployee: getEmployee
  };

  return employeeService;

  function addEmployee(data) {
    console.log('add Employee data', data);
    return commonService.apiCall(constants.employeePersist, data);
  }

  function deleteEmployee(data) {
    console.log('add Employee data', data);
    return commonService.apiCall(constants.employeeDelete, data);
  }

  function editEmployee(data) {
    console.log('add Employee data', data);
    return commonService.apiCall(constants.employeeUpdate, data);
  }

  function getEmployee(data) {
    console.log('get Employee', data);
    if (!data)
      return commonService.apiCall(constants.employeeRetrieve,undefined,true);
    else
      return commonService.apiCall(constants.employeeRetrieve, data);
  }
}