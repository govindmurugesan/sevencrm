angular
    .module('crm')
    .service('menuService', menuService);

menuService.$inject = ['constants', 'commonService'];

function menuService(constants, commonService) {

    var menusService = {
        addMenu: addMenu,
        getMenu: getMenu
    };

    return menusService;

    function addMenu(data) {
        console.log('add Menu data', data);
        return commonService.apiCall(constants.menu, data);
    }

    function getMenu() {
        console.log('get Menu');
        return commonService.apiCall(constants.menu,undefined,true);
    }
}