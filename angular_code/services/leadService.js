angular
  .module('crm')
  .service('leadService', leadService);

leadService.$inject = ['constants', 'commonService'];

function leadService(constants, commonService) {

  var leadService = {
    addLead: addLead,
    deleteLead: deleteLead,
    editLead: editLead,
    getLead: getLead
  };

  return leadService;

  function addLead(data) {
    console.log('add Lead data', data);
    return commonService.apiCall(constants.leadPersist, data);
  }

  function deleteLead(data) {
    console.log('add Lead data', data);
    return commonService.apiCall(constants.leadDelete, data);
  }

  function editLead(data) {
    console.log('add Lead data', data);
    return commonService.apiCall(constants.leadUpdate, data);
  }

  function getLead(data) {
    console.log('get Lead', data);
    if (!data)
      return commonService.apiCall(constants.leadRetrieve,undefined,true);
    else
      return commonService.apiCall(constants.leadRetrieve, data);
  }
}
