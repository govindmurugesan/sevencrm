angular
    .module('crm')
    .service('loginService', loginService);

loginService.$inject = ['constants', 'commonService'];

function loginService(constants, commonService) {

    var loginsService = {
        addLogin: addLogin,
        getLogin: getLogin
    };

    return loginsService;

    function addLogin(data) {
        console.log('add Login data', data);
        return commonService.apiCall(constants.login, data);
    }

    function getLogin() {
        console.log('get Login');
        return commonService.apiCall(constants.login,undefined,true);
    }
}