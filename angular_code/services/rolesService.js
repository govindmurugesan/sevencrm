angular
  .module('crm')
  .service('rolesService', rolesService);

rolesService.$inject = ['constants', 'commonService'];

function rolesService(constants, commonService) {

  var roleService = {
    addRole: addRole,
    deleteRole: deleteRole,
    editRole: editRole,
    getRole: getRole
  };

  return roleService;

  function addRole(data) {
    console.log('Add Role', data);
    return commonService.apiCall(constants.rolePersist, data);
  }

  function deleteRole(data) {
    console.log('Delete Role',data);
    return commonService.apiCall(constants.roleDelete, data);
  }

  function editRole(data) {
    console.log('Edit Role',data);
    return commonService.apiCall(constants.roleUpdate, data);
  }

  function getRole(data) {
    console.log('Get Role',data);
    if (!data)
      return commonService.apiCall(constants.roleRetrieve,undefined,true);
    else
      return commonService.apiCall(constants.roleRetrieve, data);
  }
}
