angular
  .module('crm')
  .service('userService', userService);

userService.$inject = ['constants', 'commonService'];

function userService(constants, commonService) {

  var usersService = {
    addUser: addUser,
    deleteUser: deleteUser,
    editUser: editUser,
    getUser: getUser
  };

  return usersService;

  function addUser(data) {
    console.log('add User data', data);
    return commonService.apiCall(constants.userPersist, data);
  }

  function deleteUser(data) {
    console.log('add User data', data);
    return commonService.apiCall(constants.userDelete, data);
  }

  function editUser(data) {
    console.log('add User data', data);
    return commonService.apiCall(constants.userUpdate, data);
  }

  function getUser(data) {
    console.log('get User', data);
    if (!data)
      return commonService.apiCall(constants.userRetrieve,undefined,true);
    else
      return commonService.apiCall(constants.userRetrieve, data);
  }
}
