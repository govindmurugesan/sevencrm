angular
    .module('crm')
    .service('configurationService', configurationService);

configurationService.$inject = ['constants', 'commonService'];

function configurationService(constants, commonService) {

    var configurationsService = {
        addConfiguration: addConfiguration,
        getConfiguration: getConfiguration
    };

    return configurationsService;

    function addConfiguration(data) {
        console.log('add Configuration data', data);
        return commonService.apiCall(constants.configuration, data);
    }

    function getConfiguration() {
        console.log('get Configuration');
        return commonService.apiCall(constants.configuration,undefined,true);
    }
}