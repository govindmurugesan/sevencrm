angular.module('crm')
    .config(function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'templates/login.html',
                controller: 'loginController as loginControllerScope'
            })
            .when('/employees', {
                templateUrl: 'templates/employees.html',
                controller: 'employeeController as employeeControllerScope'
            })
            .when('/addEmployee', {
                templateUrl: 'templates/addEmployee.html',
                controller: 'employeeController as employeeControllerScope'
            })
            .when('/editEmployee/:employeeID', {
                templateUrl: 'templates/addEmployee.html',
                controller: 'employeeController as employeeControllerScope'
            })
            .when('/viewEmployee', {
                templateUrl: 'templates/viewEmployee.html',
                controller: 'employeeController as employeeControllerScope'
            })
            .when('/customers', {
                templateUrl: 'templates/customers.html',
                controller: 'customerController as customerControllerScope'
            })
            .when('/addCustomer', {
                templateUrl: 'templates/addCustomer.html',
                controller: 'customerController as customerControllerScope'
            })
            .when('/editCustomer/:customerID', {
                templateUrl: 'templates/addCustomer.html',
                controller: 'customerController as customerControllerScope'
            })
            .when('/viewCustomer', {
                templateUrl: 'templates/viewCustomer.html',
                controller: 'customerController as customerControllerScope'
            }) 
            .when('/contacts', {
                templateUrl: 'templates/contacts.html',
                controller: 'contactController as contactControllerScope'
            })
            .when('/addContact', {
                templateUrl: 'templates/addContact.html',
                controller: 'contactController as contactControllerScope'
            })
            .when('/editContact/:contactID', {
                templateUrl: 'templates/addContact.html',
                controller: 'contactController as contactControllerScope'
            })
            .when('/viewContact', {
                templateUrl: 'templates/viewContact.html',
                controller: 'contactController as contactControllerScope'
            })
            .when('/dashboard', {
                templateUrl: 'templates/dashboard.html',
                controller: 'dashboardController as dashboardControllerScope'
            })
            .when('/configuration', {
                templateUrl: 'templates/configuration.html',
                controller: 'configurationController as configurationControllerScope'
            })
            .when('/roles', {
                templateUrl: 'templates/roles.html',
                controller: 'rolesController as rolesControllerScope'
            })
            .when('/users', {
                templateUrl: 'templates/users.html',
                controller: 'usersController as usersControllerScope'
            })
            .when('/companyInformation', {
                templateUrl: 'templates/companyInformation.html',
                controller: 'companyInformationController as companyInformationControllerScope'
            })
            .when('/reports', {
                templateUrl: 'templates/reports.html',
                controller: 'reportController as reportControllerScope'
            })
            .when('/employeeLeads', {
                templateUrl: 'templates/employeeLeads.html',
                controller: 'employeeController as employeeControllerScope'
            })
            .when('/leads', {
                templateUrl: 'templates/leads.html',
                controller: 'leadController as leadControllerScope'
            })
            .when('/addLead', {
                templateUrl: 'templates/addLead.html',
                controller: 'leadController as leadControllerScope'
            })
            .when('/viewLeads', {
                templateUrl: 'templates/viewLead.html',
                controller: 'leadController as leadControllerScope'
            })
            .when('/transferLead', {
                templateUrl: 'templates/transferLead.html',
                controller: 'leadController as leadControllerScope'
            })
            .when('/addTransferLead', {
                templateUrl: 'templates/addTransferLead.html',
                controller: 'leadController as leadControllerScope'
            })
            .when('/reCategorise', {
                templateUrl: 'templates/reCategorise.html',
                controller: 'reCategoriseController as reCategoriseControllerScope'
            })
            .when('/vendors', {
                templateUrl: 'templates/vendors.html',
                controller: 'vendorController as vendorControllerScope'
            })
            .when('/viewVendor', {
                templateUrl: 'templates/viewVendor.html',
                controller: 'vendorController as vendorControllerScope'
            })
            .when('/addVendor', {
                templateUrl: 'templates/addVendor.html',
                controller: 'vendorController as vendorControllerScope'
            })
            .when('/bills', {
                templateUrl: 'templates/bills.html',
                controller: 'billController as billControllerScope'
            })
            .when('/addBill', {
                templateUrl: 'templates/addBill.html',
                controller: 'billController as billControllerScope'
            })
            .when('/viewBill', {
                templateUrl: 'templates/viewBill.html',
                controller: 'billController as billControllerScope'
            })
            .when('/HR', {
                templateUrl: 'templates/HR.html',
                controller: 'hrController as hrControllerScope'
            })
            .when('/HRPolicy', {
                templateUrl: 'templates/HR Policy.html',
                controller: 'hrController as hrControllerScope'
            })
            .when('/Administration', {
                templateUrl: 'templates/Administration.html',
                controller: 'adminstrationController as adminstrationControllerScope'
            })
            .when('/projects', {
                templateUrl: 'templates/projects.html',
                controller: 'projectController as projectControllerScope'
            })
            .when('/addProject', {
                templateUrl: 'templates/addProject.html',
                controller: 'projectController as projectControllerScope'
            })
            .when('/editProject/:projectID', {
                templateUrl: 'templates/addProject.html',
                controller: 'projectController as projectControllerScope'
            })
            .when('/viewProject', {
                templateUrl: 'templates/viewProject.html',
                controller: 'projectController as projectControllerScope'
            })
            .when('/Reports', {
                templateUrl: 'templates/Reports.html',
                controller: 'reportController as reportControllerScope'
            })
            .when('/menu', {
                templateUrl: 'templates/menu.html',
                controller: 'menuController as menuControllerScope'
            })

            .otherwise({ redirectTo: '/' });

        // $locationProvider.html5Mode({
        //     enabled: true,
        //     requireBase: false
        // });
    });