(function() {
  'use strict';

  var appUrl = "http://192.168.0.116:8181/api/";

  var apiRoutesUrl = {
    // customer
    customerPersist: appUrl + 'customer/persist/',
    customerRetrieve: appUrl + 'customer/retrieve/',
    customerUpdate: appUrl + 'customer/update/',
    customerDelete: appUrl + 'customer/delete/',

    // contact
    contactPersist: appUrl + 'contact/persist/',
    contactRetrieve: appUrl + 'contact/retrieve/',
    contactUpdate: appUrl + 'contact/update/',
    contactDelete: appUrl + 'contact/delete/',

    // employee
    employeePersist: appUrl + 'employee/persist/',
    employeeRetrieve: appUrl + 'employee/retrieve/',
    employeeUpdate: appUrl + 'employee/update/',
    employeeDelete: appUrl + 'employee/delete/',

    // lead
    leadPersist: appUrl + 'lead/persist/',
    leadRetrieve: appUrl + 'lead/retrieve/',
    leadUpdate: appUrl + 'lead/update/',
    leadDelete: appUrl + 'lead/delete/',

    // project
    projectPersist: appUrl + 'project/persist/',
    projectRetrieve: appUrl + 'project/retrieve/',
    projectUpdate: appUrl + 'project/update/',
    projectDelete: appUrl + 'project/delete/',

    // role
    rolePersist: appUrl + 'role/persist/',
    roleRetrieve: appUrl + 'role/retrieve/',
    roleUpdate: appUrl + 'role/update/',
    roleDelete: appUrl + 'role/delete/',

    // user
    userPersist: appUrl + 'user/persist/',
    userRetrieve: appUrl + 'user/retrieve/',
    userUpdate: appUrl + 'user/update/',
    userDelete: appUrl + 'user/delete/',

    // vendor
    vendorPersist: appUrl + 'vendor/persist/',
    vendorRetrieve: appUrl + 'vendor/retrieve/',
    vendorUpdate: appUrl + 'vendor/update/',
    vendorDelete: appUrl + 'vendor/delete/',


  };

  angular.module('crm').constant('constants', apiRoutesUrl);

})();
